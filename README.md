# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://jaka4@bitbucket.org/jaka4/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/jaka4/stroboskop/commits/246381b9f20e34b974adaf008c1185ae26efe318

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/jaka4/stroboskop/commits/a193c885a59445416f7ad1637ea51f76cbaa990a

Naloga 6.3.2:
https://bitbucket.org/jaka4/stroboskop/commits/8e8e62238084f124e9a6b3d910490ad26b7dd34a

Naloga 6.3.3:
https://bitbucket.org/jaka4/stroboskop/commits/88a9524016804438d67ffca785b5aaf4714850aa

Naloga 6.3.4:
https://bitbucket.org/jaka4/stroboskop/commits/8a41646af79e8ef770115d92dc2b44d4e6673058

Naloga 6.3.5:

```
git checkout master 
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/jaka4/stroboskop/commits/f4e77eddd9f84f142e4c0c8b0bbe0493cf545266

Naloga 6.4.2:
https://bitbucket.org/jaka4/stroboskop/commits/781bf8725d44214349c125dbfa5b5e5811a7e5d7

Naloga 6.4.3:
https://bitbucket.org/jaka4/stroboskop/commits/6ce1f9f186277ed4be340cacea9441772d4d1995

Naloga 6.4.4:
https://bitbucket.org/jaka4/stroboskop/commits/fd57fc41fd32d0f58dd74ded95a243189fcf32cf